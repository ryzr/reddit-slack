<?php

namespace App\Providers;

use App\Http\Controllers\SlackController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::post(config('services.slack.hook_uri'), [SlackController::class, 'action'])->middleware('api');
    }
}
