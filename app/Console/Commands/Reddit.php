<?php

namespace App\Console\Commands;

use App\Reddit\SubredditImageFetcher;
use Illuminate\Console\Command;

class Reddit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reddit {subreddit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Posts a random image from the specified subreddit to Slack';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line((new SubredditImageFetcher)->getRandomImage($this->argument('subreddit')));
    }
}
