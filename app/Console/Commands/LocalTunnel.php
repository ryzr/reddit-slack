<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PharIo\Manifest\InvalidUrlException;

class LocalTunnel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'localtunnel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        passthru($this->localTunnelCommand());
    }

    private function localTunnelCommand()
    {
        $appUrl = parse_url(config('app.url'));

        if (! isset($appUrl['host'])) {
            throw new InvalidUrlException('Could not parse a host from specified APP_URL environment variable.');
        }

        return sprintf('lt --subdomain %s --local-host %s --port 80 --print-requests', str_slug(config('app.name')), $appUrl['host']);
    }
}
