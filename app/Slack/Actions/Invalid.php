<?php

namespace App\Slack\Actions;

use Symfony\Component\HttpFoundation\Response;

class Invalid extends AbstractAction
{
    public function handle(array $payload) : Response
    {
        return response()->make();
    }
}
