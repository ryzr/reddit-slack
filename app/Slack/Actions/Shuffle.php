<?php

namespace App\Slack\Actions;

use App\Reddit\SubredditImageFetcher;
use App\Slack\Messages\ImageSelector;
use Symfony\Component\HttpFoundation\Response;

class Shuffle extends AbstractAction
{
    public function handle(array $payload): Response
    {
        return response()->json([
            'response_type'    => 'ephemeral',
            'replace_original' => true,
            'text'             => '',
            'attachments'      => [
                (new ImageSelector)->build([
                    'subreddit' => $payload['callback_id'],
                    'image_url' => (new SubredditImageFetcher)->getRandomImage($payload['callback_id']),
                ])->toArray(),
            ],
        ]);
    }
}
