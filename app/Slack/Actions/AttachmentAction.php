<?php

namespace App\Slack\Actions;

use Spatie\SlashCommand\AttachmentAction as BaseAttachmentAction;

class AttachmentAction extends BaseAttachmentAction
{
    /**
     * Convert this action to its array representation.
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [
            'name'  => $this->name,
            'text'  => $this->text,
            'type'  => $this->type,
            'value' => $this->value,
            'style' => $this->style,
        ];

        if ($this->confirmation) {
            $array['confirm'] = $this->confirmation;
        }

        return $array;
    }
}
