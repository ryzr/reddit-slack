<?php

namespace App\Slack\Actions;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAction
{
    protected $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => 'https://slack.com/api/',
            'headers'  => [
                'Authorization' => 'Bearer ' . config('services.slack.oauth_access_token'),
            ],
        ]);
    }

    public abstract function handle(array $payload) : Response;
}
