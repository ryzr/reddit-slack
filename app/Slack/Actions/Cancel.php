<?php

namespace App\Slack\Actions;

use Symfony\Component\HttpFoundation\Response;

class Cancel extends AbstractAction
{
    public function handle(array $payload): Response
    {
        return response()->json([
            'response_type'    => 'ephemeral',
            'replace_original' => true,
            'delete_original'  => true,
            'text'             => '',
        ]);
    }
}
