<?php

namespace App\Slack\Actions;

use GuzzleHttp\RequestOptions;
use Spatie\SlashCommand\Attachment;
use Symfony\Component\HttpFoundation\Response;

class Send extends AbstractAction
{
    public function handle(array $payload): Response
    {
        $this->httpClient->post('chat.postMessage', [
            'headers'  => [
                'Authorization' => 'Bearer ' . config('services.slack.oauth_access_token'),
            ],
            RequestOptions::JSON => [
                'token'       => config('services.slack.oauth_access_token'),
                'channel'     => $payload['channel']['id'],
                'as_user'     => true,
                'attachments' => [
                    Attachment::create()
                        ->setText('Posted using /reddit')
                        ->setTitle($payload['callback_id'])
                        ->setTitleLink(html_entity_decode($payload['actions'][0]['value']))
                        ->setImageUrl(html_entity_decode($payload['actions'][0]['value']))
                        ->toArray()
                ],
            ],
        ]);

        return response()->json([
            'response_type'    => 'ephemeral',
            'replace_original' => true,
            'delete_original'  => true,
            'text'             => '',
        ]);
    }
}
