<?php

namespace App\Slack\Commands;

use App\Reddit\RedditFetcherException;
use App\Reddit\SubredditImageFetcher;
use App\Slack\Messages\ImageSelector;
use Spatie\SlashCommand\Handlers\BaseHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;

class Reddit extends BaseHandler
{

    /**
     * If this function returns true, the handle method will get called.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return bool
     */
    public function canHandle(Request $request): bool
    {
        return $request->command == env('SLACK_COMMAND_PREFIX');
    }

    /**
     * Handle the given request. Remember that Slack expects a response
     * within three seconds after the slash command was issued. If
     * there is more time needed, dispatch a job.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     */

    public function handle(Request $request): Response
    {
        try {
            return (new ImageSelector)->send($this, [
                'subreddit' => $request->text,
                'image_url' => (new SubredditImageFetcher)->getRandomImage($request->text),
            ]);
        } catch (RedditFetcherException $e) {
            return $this->respondToSlack($e->getMessage())->displayResponseToUserWhoTypedCommand();
        } catch (\Exception $e) {
            return $this->respondToSlack("Whoops! Something went wrong. Please try again.")->displayResponseToUserWhoTypedCommand();
        }
    }
}
