<?php

namespace App\Slack\Messages;

use Spatie\SlashCommand\Attachment;
use App\Slack\Actions\AttachmentAction;
use Spatie\SlashCommand\HandlesSlashCommand;

class ImageSelector extends AbstractMessage
{
    public function send(HandlesSlashCommand $command, array $params = [])
    {
        return $command->respondToSlack("")
            ->withAttachment($this->build($params))
            ->displayResponseToUserWhoTypedCommand();
    }

    public function build(array $params = [])
    {
        $sendButton = AttachmentAction::create('send', 'Send', 'button')->setStyle('primary')->setValue($params['image_url']);
        $shuffleButton = AttachmentAction::create('shuffle', 'Shuffle', 'button');
        $cancelButton = AttachmentAction::create('cancel', 'Cancel', 'button');

        return Attachment::create()
            ->setTitle($params['subreddit'])
            ->setTitleLink(html_entity_decode($params['image_url']))
            ->setImageUrl(html_entity_decode($params['image_url']))
            ->setCallbackId($params['subreddit'])
            ->addAction($sendButton)
            ->addAction($shuffleButton)
            ->addAction($cancelButton);
    }
}
