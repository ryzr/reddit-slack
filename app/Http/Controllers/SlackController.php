<?php

namespace App\Http\Controllers;

use App\Slack\Actions\Invalid;
use Illuminate\Http\Request;

class SlackController
{
    public function action(Request $request)
    {
        $payload = json_decode($request->input('payload'), true);

        if ($payload && isset($payload['actions'][0])) {
            $actionHandler = "\\App\\Slack\\Actions\\" . ucfirst($payload['actions'][0]['name']);

            if (class_exists($actionHandler)) {
                return (new $actionHandler)->handle($payload);
            }
        }

        return (new Invalid)->handle($payload);
    }
}
