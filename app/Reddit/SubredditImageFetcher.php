<?php

namespace App\Reddit;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SubredditImageFetcher
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $http;

    public function __construct()
    {
        $this->http = new Client([
            'base_uri' => 'https://reddit.com/r/',
        ]);
    }

    /**
     *
     * @param string $subreddit
     *
     * @throws \App\Reddit\RedditFetcherException
     * @return bool
     */
    public function getRandomImage(string $subreddit)
    {
        $images = $this->fetch($subreddit);

        if (! count($images)) {
            throw new RedditFetcherException("Could not find any images for subreddit: $subreddit");
        }

        if (isset($images['error'])) {
            throw new RedditFetcherException($images['error']);
        }

        return count($images) ? $images[array_rand($images, 1)] : false;
    }

    /**
     * @param string $subreddit
     *
     * @return mixed
     */
    public function fetch(string $subreddit)
    {
        return app('cache')->remember('reddit-subreddit-' . $subreddit, 15, function () use ($subreddit) {
            try {
                $response = $this->http->request('GET', sprintf('/r/%s/top.json', $subreddit), [
                    'headers' => $this->getHeaders(),
                    'query'   => ['limit' => 100, 't' => 'week'],
                ]);

                $listing = json_decode($response->getBody()->getContents(), true);

                return collect($listing['data']['children'] ?? [])
                    ->map(function ($post) {
                        return html_entity_decode($post['data']['preview']['images'][0]['source']['url'] ?? null);
                    })
                    ->filter(function ($image) {
                        return ! ! $image;
                    })
                    ->toArray();
            } catch (ClientException $e) {
                $responseJson = json_decode($e->getResponse()->getBody()->getContents());

                if ($responseJson && isset($responseJson->reason)) {
                    switch ($responseJson->reason) {
                        case 'private':
                            return ["error" => "Could not access private subreddit: $subreddit"];
                        case 'banned':
                            return ["error" => "Could not access banned subreddit: $subreddit"];
                    }
                }

                throw $e;
            }
        });
    }

    protected function getHeaders()
    {
        return [
            'User-Agent' => 'SlackBot:' . env('REDDIT_CLIENT_ID') . ':1.0.0 (by /u/' . env('REDDIT_USERNAME') . ')',
        ];
    }
}
